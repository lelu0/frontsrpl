﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FrontSRPL
{
    public class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Subtitle { get; set; }
        public string Image { get; set; }

        public News() { }
        public News(int id, string t, string s, string i)
        {
            Id = id;
            Title = t;
            Subtitle = s;
            Image = i;
        }
    }
}