﻿using FrontSRPL.Models;
using FrontSRPL.Views.Shared;
using Simple.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FrontSRPL.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View(new PageContainer());
        }
    }
}