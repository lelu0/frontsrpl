﻿using FrontSRPL.Views.Shared;
using Simple.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FrontSRPL.Models
{
    public class PageContainer
    {
        public List<News> mNews;
        public List<League> mLeagues;
        public List<Image> mImages;

        public PageContainer()
        {
            mNews = getNews();
            mLeagues = getLeagues();
            mImages = getImages();
        }

        List<News> getNews()
        {
            List<News> nList = new List<News>();
            var db = Database.OpenConnection(_Globals.cstr);
            IEnumerable<dynamic> data = db.news.All().OrderByDescending(db.news.idnews);
            foreach(var i in data)
            {
                nList.Add(new News(i.idnews, i.title, i.subtitle, i.image));
            }
            return nList;
        }

        List<League> getLeagues()
        {
            List<League> nList = new List<League>();
            var db = Database.OpenConnection(_Globals.cstr);
            IEnumerable<dynamic> data = db.leagues.All().OrderByDescending(db.leagues.idleagues);
            foreach (var i in data)
            {
                nList.Add(new League(i.name, i.link, i.image));
            }
            return nList;
        }

        List<Image> getImages()
        {
            List<Image> nList = new List<Image>();
            var db = Database.OpenConnection(_Globals.cstr);
            IEnumerable<dynamic> data = db.images.All().OrderByDescending(db.images.idimages);
            foreach (var i in data)
            {
                nList.Add(new Image(i.link, i.desc));
            }
            return nList;
        }

    }
}