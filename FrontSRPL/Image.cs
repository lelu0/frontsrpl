﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FrontSRPL
{
    public class Image
    {
        
        public string link;
        public string desc;

        public Image( string l, string d)
        {
            
            link = l;
            desc = d;
        }
    }
}