﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FrontSRPL
{
    public class League
    {
        public string name;
        public string link;
        public string image;

        public League(string n, string l, string i)
        {
            name = n;
            link = l;
            image = i;
        }
     
    }
}